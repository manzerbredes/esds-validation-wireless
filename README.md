# Wireless Validation Experiments of ESDS

#### Scientific Publication
These results led to a publication in the DCOSS-IoT 2023 workshop. The paper is entitled: *"Validation of ESDS Using Epidemic-Based Data Dissemination Algorithms"*

#### Folders architecture:
- `probabilitic_flooding/` Experiments regarding the *PF* algorithm
- `susceptible_infected_recovered/` Experiments regarding the *SIR* algorithm
- `susceptible_infected_recovered_par/` Same as previous folder but runs the experiments for each initiator node in parallel

#### Dependencies
Run `./setup.sh` to install the dependencies:
- [ESDS](https://gitlab.com/manzerbredes/esds)
- [SimGrid](https://framagit.org/simgrid/simgrid)

#### How to run an experiment from folder `X`
- Change to directory `X`
- Execute `run.sh` to run the experiments
- Results are placed in `results/results_(sg|ys).csv`
- Execution duration is stored in `results/runtime.txt` and contains 2 timestamps:
  1. Start of the experiment execution (line 1)
  2. End of the experiment execution (line 2)
- Analyze results with `results/analysis.R`
- Generated figures (the one presented in the paper) are placed into `results/figures/`

####  Side notes
- Every variable, file etc.. containing `ys` or `_ys` are related to the ESDS simulator
- Analysis scripts use [renv](https://rstudio.github.io/renv/articles/renv.html) to help for reproducibility
- Duration of *PF* experiments on our machine is *74h 1m 59s* (Intel(R) Core(TM) i5-8500T CPU @ 2.10GHz)
- Duration of *SIR* experiments (parallel one) on our machine is *24h 21m 01s* (Intel(R) Xeon(R) Silver 4114 CPU @ 2.20GHz)
- You can change *maxpar* in `susceptible_infected_recovered_par/run.sh` according to the number of core of your machine
- `network_adjacency_matrix.txt` contains the network adjacency matrix used in [CaBIUs paper](https://www.mdpi.com/2079-9292/9/3/454)
- Contact [me](http://loicguegan.com/) if you need help
