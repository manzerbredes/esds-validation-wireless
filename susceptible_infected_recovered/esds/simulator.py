#!/usr/bin/env python3

# Load ESDS
import sys,random
import esds
import numpy as np

# Simulations arguments
timeout=60000  # Time at which nodes will terminate 
bandwidth=8 # Bandwidth in bps
max_node_degree=6
time_steps=5
beta=100 # Infection rate
alpha=100 # Recovery rate
bias=32
run=0
initiator=8 # Default is 8
randnode_gen=random.Random(2347)
bias_gen=random.Random(121)

# Check command line arguments
if len(sys.argv) > 1:
    beta=int(sys.argv[1])
    alpha=int(sys.argv[2])
    initiator=int(sys.argv[3])

# Initialize network
B=np.full((25,25),0)
L=np.full((25,25),0)


B = np.loadtxt("network_adjacency_matrix.txt")
np.fill_diagonal(B,1)

# Initialize simulator
s=esds.Simulator({"wlan0":{"bandwidth":B*8, "latency":L, "is_wired": False}})

# Create nodes
for n in range(0,25): 
    s.create_node("node",interfaces=["wlan0"],args=(timeout,beta,alpha,time_steps,run,initiator))

# Infos
print("Network average degree is {}".format(np.mean(np.sum(B,axis=0))))
print("run={} beta={} alpha={}".format(run,beta,alpha))

# Run the simulation
s.run(interferences=False)
