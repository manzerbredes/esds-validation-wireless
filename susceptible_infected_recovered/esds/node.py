#!/usr/bin/env python

######################
#     _    ____ ___  #
#    / \  |  _ \_ _| #
#   / _ \ | |_) | |  #
#  / ___ \|  __/| |  #
# /_/   \_\_|  |___| #
#                    #
######################
# api.args                                       # Contains node arguments
# api.send(interface, data,size, dst)            # If interface is "wlan0" dst is not used
# api.sendt(interface, data,size, dst,timeout)   # Similar to api.send() but with timeout
# api.receive(interface)                         # Receive the data by returning the following tuple (code,data) if code is 0 receive succeed
# api.receivet(interface,timeout)                # Similar to api.receive() but with timeout
# api.read("clock")                              # Get current simulated time
# api.log(msg)                                   # Print log in the simulation console
# api.wait(duration)                             # Wait for "duration" seconds of simulated time
# api.turn_off(duration)                         # Turn the node off for "duration" seconds (no data can be receive during this time period)

import random
import numpy as np

randbeta = np.loadtxt("./random_beta_ys.txt",ndmin=2)
randalpha = np.loadtxt("./random_alpha_ys.txt",ndmin=2)

def execute(api):
    timeout, beta,alpha,time_steps,run, initiator=api.args
    infected=(api.node_id==initiator)
    rcv_count=0
    ntx=0
    api.log(str(randbeta[api.node_id]))
    # Receive until contaminated or simulation ends
    if not(infected):
        while(api.read("clock")<timeout):
            code, data=api.receivet("wlan0",timeout)
            if code == 0:
                infected=randbeta[api.node_id][rcv_count]<beta
                rcv_count+=1          
                if infected:
                    api.log("Node {} is infected at rcv_count={}".format(api.node_id,rcv_count-1))
                    break
                else:
                    api.log("Infection failed rand={} but beta={}".format(randbeta[api.node_id][rcv_count-1],beta))

    # Continue until recovered
    try_nb=0
    if infected:
        api.log("Infected")
    while(infected):
        api.send("wlan0","Hello",1,None)
        ntx+=1
        api.wait(1) # Wait 1 unit of time
        infected=not(randalpha[api.node_id][try_nb]<alpha)
        try_nb+=1
        if not(infected):
            api.log("Recovered at try_nb={} rand={} alpha={}".format(try_nb-1,randalpha[api.node_id][try_nb-1],alpha))
            break;
        if try_nb >= len(randalpha[api.node_id]):
            api.log("Too much alpha try! Switch to recovered...")
            infected=False
            break
            
    api.log("Number of tx "+str(ntx))
    api.log("rcv_count={}".format(rcv_count))
