#!/usr/bin/env bash

set -e

##### Arguments
wai=$(dirname $(readlink -f "$0")) # Current script directory
nrun=1000

##### Paths
randrun="./randrun.py"
results_sg="results/results_sg.csv"
results_ys="results/results_ys.csv"
results_time="results/runtime.txt"
sim_sg="make -C ./simgrid/ run"
sim_ys="./esds/simulator.py"
randbeta_sg="random_beta_sg.txt"
randalpha_sg="random_alpha_sg.txt"
randbeta_ys="random_beta_ys.txt"
randalpha_ys="random_alpha_ys.txt"
max_recovery_try=500
max_beta_try=500
seed_ys=15
seed_sg=15


# Init results files
echo "beta,alpha,initiator,seed,run_number,node,ntx" > $results_ys
echo "beta,alpha,initiator,seed,run_number,node,ntx" > $results_sg
echo $(date "+%s") > "$results_time"


simulate () {
    initiator=$1
    beta=$2
    alpha=$3

    for run in $(seq 0 $(( nrun - 1)))
    do
        
        # Generate random streams
        $randrun $randbeta_sg 25 $max_beta_try $((seed_sg + run + 1))
        $randrun $randalpha_sg 25 $max_recovery_try $((seed_sg + run + 1))

        $randrun $randbeta_ys 25 $max_beta_try $((seed_sg + run + 1))
        $randrun $randalpha_ys 25 $max_recovery_try $((seed_ys + run + 1))

        # ESDS
        echo "ESDS - Run seed=$seed_ys beta=$beta alpha=$alpha run_number=$run initiator=$initiator"
        ys_out=$($sim_ys $beta $alpha $initiator)
        nodes=$(echo "$ys_out"| awk '/Receive.*wlan0/{print $1}' | grep -Po n[0-9]+ | sed "s/^n//g"|sort|uniq|grep -v ^${initiator}$)
        ntx=$(echo "$ys_out"|grep -Eo "Number of tx [0-9]+"|awk '{s+=$4} END {print s}')
        for node in $nodes
        do
            echo $beta,$alpha,$initiator,$seed_ys,$run,$node,$ntx >> $results_ys
        done
        # Add initiator the the receiver list for each simulation
        echo $beta,$alpha,$initiator,$seed_ys,$run,$initiator,$ntx >> $results_ys
        
        # Simgrid
        echo "Simgrid - Run seed=$seed_sg beta=$beta alpha=$alpha run_number=$run initiator=$initiator"
        sg_out=$($sim_sg beta=$beta alpha=$alpha initiator=$initiator 2>&1)
        nodes=$(echo "$sg_out" | awk '/Received/{print $1}' | grep -Po n[0-9]+ | sed "s/^n//g"|sort|uniq|grep -v ^${initiator}$)
        ntx=$(echo "$sg_out"|grep -Eo "Number of tx [0-9]+"|grep -Eo "[0-9]+")
        for node in $nodes
        do
            echo $beta,$alpha,$initiator,$seed_sg,$run,$node,$ntx >> $results_sg
        done
        echo $beta,$alpha,$initiator,$seed_sg,$run,$initiator,$ntx >> $results_sg
    done
    
    diff $results_sg $results_ys &> /dev/null || { echo ""; echo "!!!!! Diff !!!!!" && exit 0; }
    
}


##### Infos #####
# alpha is the node recovery rate
# beta is the infection rate
initiator=$(cat ${wai}/initiator.txt)
for vars in "0.3 0.6" "0.4 0.6" "0.5 0.6" "0.55 0.6" "0.6 0.6" "0.65 0.6" "0.7 0.6" "0.7 0.5" "0.8 0.5"
do
    vars_arr=($vars)
    beta=$(echo ${vars_arr[0]} | awk '{printf $1*100}')
    alpha=$(echo ${vars_arr[1]} | awk '{printf $1*100}')
    simulate $initiator $beta $alpha
done


echo $(date "+%s") >> "$results_time"
