#include <simgrid/s4u.hpp>
#include <simgrid/s4u/Mailbox.hpp>
#include <simgrid/s4u/Host.hpp>
#include <simgrid/plugins/energy.h>
#include <vector>
#include <xbt/log.h>

#include <string>
#include <sstream>
#include <stdlib.h>     /* srand, rand */
#include "simgrid/s4u/Actor.hpp"
#include <fstream>
#include <iostream>


/// @brief Required by SimGrid
XBT_LOG_NEW_DEFAULT_CATEGORY(simulator, "[DAO] Data dissemination");

static void obs_node(std::vector<std::string> args);
typedef unsigned int u32;
std::vector<std::vector<int>> randbeta;
std::vector<std::vector<int>> randalpha;
std::vector<std::vector<int>> receivers;
int ntx=0;

void LoadRandom(std::string filepath, std::vector<std::vector<int>> *array) {
  std::ifstream myfile(filepath);

  std::string myline;
  while ( std::getline (myfile,myline)) { // equivalent to myfile.good()
    std::istringstream iss(myline);
    std::vector<int> run;
    while(!iss.eof()){
      std::string current_word; 
      iss >> current_word;
      run.push_back(std::atoi(current_word.c_str()));
    }
    array->push_back(run);
  }
}

void LoadMatrix() {
  std::vector<std::vector<int>> rand;
  std::ifstream myfile("../network_adjacency_matrix.txt");

  std::string myline;
  while ( std::getline (myfile,myline)) { // equivalent to myfile.good()
    std::istringstream iss(myline);
    std::vector<int> node;
    for(int i=0;i<25;i++){
      std::string current_word; 
      iss >> current_word;
      if(std::atoi(current_word.c_str()) == 1) 
        node.push_back(i);
    }
    receivers.push_back(node);
  }
}



/**
 * No arguments are require (cf inputs.json)
 */
int main(int argc, char **argv) {

    // Build engine
    simgrid::s4u::Engine engine(&argc, argv);
    engine.load_platform("platform.xml");

    // Headline
    XBT_INFO("-------------------------------------------------");
    XBT_INFO("Sarting loosely coupled data dissemination experiments");
    XBT_INFO("-------------------------------------------------");
    LoadMatrix();
    LoadRandom("../random_beta_sg.txt", &randbeta);
    LoadRandom("../random_alpha_sg.txt", &randalpha);

    // Init all nodes actors
    u32 nON=simgrid::s4u::Engine::get_instance()->get_host_count();

    u32 beta=std::atoi(argv[1]);
    u32 alpha=std::atoi(argv[2]);
    u32 initiator=std::atoi(argv[3]);
    
    for(u32 i=0;i<nON;i++){
        std::vector<std::string> args; // No args
        args.push_back(std::to_string(i));
        args.push_back(std::to_string(beta));
        args.push_back(std::to_string(alpha));
        args.push_back(std::to_string(initiator));
        std::ostringstream ss;
        ss<< "n" <<i;
        simgrid::s4u::Actor::create("n", simgrid::s4u::Host::by_name(ss.str()), obs_node, args);
    }

    // Launch the simulation
    engine.run();
    XBT_INFO("Number of tx %d",ntx);
    XBT_INFO("Simulation tooks %fs", simgrid::s4u::Engine::get_clock());
    return (0);
}


/**
 * This is the brain behind each node
 */
static void obs_node(std::vector<std::string> args) {
  const char* c_id=args[0].c_str();
  int id=std::atoi(c_id);
  const char* c_beta=args[1].c_str();
  int beta=std::atoi(c_beta);
  const char* c_alpha=args[2].c_str();
  int alpha=std::atoi(c_alpha);
  const char* c_initiator=args[3].c_str();
  int initiator=std::atoi(c_initiator);
  simgrid::s4u::Mailbox *m = simgrid::s4u::Mailbox::by_name("main"+args[0]);
  int *data= new int;

  std::cout << "Node " << id << " "; 
  for(auto &n:randbeta[id]){
    std::cout << n << " ";
  }
  std::cout << std::endl;
  

  int steps=20; // Duration of a step
  int try_nb=0;
  int t=0;
  int rcv_count=0;
  bool should_forward=false;
  bool infected=initiator==id;
  bool recovered=false;
  if(infected)
    XBT_INFO("Node %d is infected",id);
  while(simgrid::s4u::Engine::get_clock() <= 100000){
    bool is_initiator_turn=t/steps<25;
    int current_id=t/steps%25;
    // Compute next step
    t+=steps;
    t/=steps;
    t*=steps;

    // If it is node turn
    if(current_id==id){
      if(infected){
          XBT_INFO("Node %d is transmitting",id);
          ntx++;
          for(auto &receiver: receivers[id]){
            simgrid::s4u::Mailbox *receiver_m = simgrid::s4u::Mailbox::by_name("main"+std::to_string(receiver));
            receiver_m->put(&data,0);
          }
        // Update infection state
        infected=!(randalpha[id][try_nb]<alpha);
        try_nb++;
        if(!infected){
          XBT_INFO("Node %d recovered at try_nb=%d",id,try_nb-1);
          recovered=true;
        }
      }
    }
    else {
      try{
        m->get<int>(steps);
        XBT_INFO("%d Received",id);
        if(!recovered and !infected){
          infected=randbeta[id][rcv_count]<beta;
          rcv_count+=1;
          if(id==16)
            XBT_INFO("bam %d",infected);
          if(infected)
            XBT_INFO("Node %d is infected at rcv_count=%d",id, rcv_count-1);
        }
      }catch(...){}
    }
    // Goto next step
    simgrid::s4u::this_actor::sleep_until(t);
  }
}
