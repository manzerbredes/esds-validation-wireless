#!/usr/bin/python

import sys, random

filepath=sys.argv[1]
m=int(sys.argv[2])
n=int(sys.argv[3])
seed=int(sys.argv[4])

random_gen=random.Random(seed)

with open(filepath, "w") as f:
    for i in range(0,m):
        for j in range(0,n):
            if j==n-1:
                f.write(str(round(random_gen.uniform(0,1)*100)))
            else:
                f.write(str(round(random_gen.uniform(0,1)*100)) + " ")
        f.write('\n')

