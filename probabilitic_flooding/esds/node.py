#!/usr/bin/env python

######################
#     _    ____ ___  #
#    / \  |  _ \_ _| #
#   / _ \ | |_) | |  #
#  / ___ \|  __/| |  #
# /_/   \_\_|  |___| #
#                    #
######################
# api.args                                       # Contains node arguments
# api.send(interface, data,size, dst)            # If interface is "wlan0" dst is not used
# api.sendt(interface, data,size, dst,timeout)   # Similar to api.send() but with timeout
# api.receive(interface)                         # Receive the data by returning the following tuple (code,data) if code is 0 receive succeed
# api.receivet(interface,timeout)                # Similar to api.receive() but with timeout
# api.read("clock")                              # Get current simulated time
# api.log(msg)                                   # Print log in the simulation console
# api.wait(duration)                             # Wait for "duration" seconds of simulated time
# api.turn_off(duration)                         # Turn the node off for "duration" seconds (no data can be receive during this time period)

import sys, random
import numpy as np
import esds.plugins.power_states as ps

rand = np.loadtxt("./random_ys.txt",ndmin=2)

def execute(api):
    timeout, q,run, initiator,data_size=api.args
    power=ps.PowerStatesComms(api)
    power.set_power("wlan0",0.4,0.16,0.16)
    ntx=0
    
    # First node that propagate the data
    if api.node_id == initiator: # Initiator node
        api.log("Start dissemination")
        api.send("wlan0","Hello",data_size,None)
        ntx+=1
    else:
        code, data=api.receivet("wlan0",timeout)
        if code == 0:
            if rand[run][api.node_id]<q:
                api.send("wlan0","Hello",data_size,None)
                ntx+=1

    clock=api.read("clock")
    if(clock<timeout):
        api.wait(timeout-clock)
    api.log("Number of tx "+str(ntx))
    # Energy report
    api.wait_end() # Wait for the end of the simulation to report energy consumption
    power.report_energy()
            
