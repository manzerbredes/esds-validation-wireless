#!/usr/bin/env python3

# Load ESDS
import sys,random
import esds
import numpy as np

# Simulations arguments
timeout=10000  # Time at which nodes will terminate 
bandwidth=50000 # Bandwidth in bps
data_size=1000000
max_node_degree=6
q=40
bias=32
run=0
node_seed=152
initiator=8 # Default is 8
randnode_gen=random.Random(2347)
bias_gen=random.Random(121)

# Check command line arguments
if len(sys.argv) > 1:
    run=int(sys.argv[1])
    q=int(sys.argv[2])
    initiator=int(sys.argv[3])

# Initialize network
B=np.full((25,25),0)
L=np.full((25,25),0)

def get_node_degree(node):
    return(np.sum(B[node,:]))

def random_connect(node):
    randnode=randnode_gen.randint(0,24)
    n=0
    while randnode == node or get_node_degree(randnode)>=max_node_degree:
        randnode=(randnode+1)%25
        n+=1
        if n>=25:
            return
    B[node,randnode]=1
    B[randnode,node]=1

# Create a random network
for i in range(0,25):
    for j in range(0,max_node_degree):
        if get_node_degree(i) >= max_node_degree:
            break
        if bias_gen.randint(0,100) < bias:
            continue
        random_connect(i)

B = np.loadtxt("network_adjacency_matrix.txt")
np.fill_diagonal(B,1) # tx and rx will have the same duration

# Initialize simulator
s=esds.Simulator({"wlan0":{"bandwidth":B*bandwidth, "latency":L, "is_wired":False}})

# Create nodes
for n in range(0,25): 
    s.create_node("node",interfaces=["wlan0"],args=(timeout,q,run,initiator,data_size))

# Infos
print("Network average degree is {}".format(np.mean(np.sum(B,axis=0))))
print("run={} q={} seed={}".format(run,q,node_seed))
rand = np.loadtxt("./random_ys.txt",ndmin=2)
print("Using run {}".format(rand[run]))

# Run the simulation
s.run(interferences=False)
