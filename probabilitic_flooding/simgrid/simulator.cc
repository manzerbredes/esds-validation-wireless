#include <simgrid/s4u.hpp>
#include <simgrid/s4u/Mailbox.hpp>
#include <simgrid/s4u/Host.hpp>
#include <simgrid/plugins/energy.h>
#include <vector>
#include <xbt/log.h>

#include <string>
#include <sstream>
#include <stdlib.h>     /* srand, rand */
#include "simgrid/s4u/Actor.hpp"
#include <fstream>
#include <iostream>

#define MODE_IDLE() simgrid::s4u::this_actor::get_host()->set_pstate(1);
#define MODE_TX() simgrid::s4u::this_actor::get_host()->set_pstate(2);
#define MODE_RX() simgrid::s4u::this_actor::get_host()->set_pstate(2);

#define DATA_SIZE 1000000
#define END_AT 1000000

/// @brief Required by SimGrid
XBT_LOG_NEW_DEFAULT_CATEGORY(simulator, "[DAO] Data dissemination");

static void obs_node(std::vector<std::string> args);
static void obs_node2(std::vector<std::string> args);
typedef unsigned int u32;
std::vector<std::vector<int>> randlist;
std::vector<std::vector<int>> receivers;
int run=0;
int ntx=0;

std::vector<std::vector<int>> LoadRandom() {
  std::vector<std::vector<int>> rand;
  std::ifstream myfile("../random_sg.txt");

  std::string myline;
  while ( std::getline (myfile,myline)) { // equivalent to myfile.good()
    std::istringstream iss(myline);
    std::vector<int> run;
    for(int i=0;i<25;i++){
      std::string current_word; 
      iss >> current_word;
      run.push_back(std::atoi(current_word.c_str()));
    }
    rand.push_back(run);
  }
  return(rand);
}

void LoadMatrix() {
  std::vector<std::vector<int>> rand;
  std::ifstream myfile("../network_adjacency_matrix.txt");

  std::string myline;
  while ( std::getline (myfile,myline)) { // equivalent to myfile.good()
    std::istringstream iss(myline);
    std::vector<int> node;
    for(int i=0;i<25;i++){
      std::string current_word; 
      iss >> current_word;
      if(std::atoi(current_word.c_str()) == 1) 
        node.push_back(i);
    }
    receivers.push_back(node);
  }
}



/**
 * No arguments are require (cf inputs.json)
 */
int main(int argc, char **argv) {

    // Build engine
    sg_host_energy_plugin_init();
    simgrid::s4u::Engine engine(&argc, argv);
    engine.load_platform("platform.xml");

    // Headline
    XBT_INFO("-------------------------------------------------");
    XBT_INFO("Sarting loosely coupled data dissemination experiments");
    XBT_INFO("-------------------------------------------------");
    LoadMatrix();
    randlist=LoadRandom();
    // Init all nodes actors
    u32 nON=simgrid::s4u::Engine::get_instance()->get_host_count();

    run=std::atoi(argv[1]);
    u32 q=std::atoi(argv[2]);
    u32 initiator=std::atoi(argv[3]);

    // Print run infos
    std::string current_run;
    for(auto &r: randlist[run]){
      current_run+=std::to_string(r)+" ";
    }
    XBT_INFO("Using run: %s",current_run.c_str());
    
    for(u32 i=0;i<nON;i++){
        std::vector<std::string> args; // No args
        args.push_back(std::to_string(i));
        args.push_back(std::to_string(q));
        args.push_back(std::to_string(initiator));
        std::ostringstream ss;
        ss<< "n" <<i;
        simgrid::s4u::Actor::create("n", simgrid::s4u::Host::by_name(ss.str()), obs_node2, args);
    }

    // Launch the simulation
    engine.run();
    XBT_INFO("Number of tx %d",ntx);
    XBT_INFO("Simulation tooks %fs", simgrid::s4u::Engine::get_clock());
    return (0);
}

/**
 * This is the brain behind each node
 */
static void obs_node(std::vector<std::string> args) {
  const char* c_id=args[0].c_str();
  int id=std::atoi(c_id);
  const char* c_q=args[1].c_str();
  int q=std::atoi(c_q);
  const char* c_initiator=args[2].c_str();
  int initiator=std::atoi(c_initiator);
  simgrid::s4u::Mailbox *m = simgrid::s4u::Mailbox::by_name("main"+args[0]);
  int *data= new int;

  bool forwarded=false;
  if(id==initiator){
    forwarded=true;
    XBT_INFO("I am the initiator");
    for(auto &receiver: receivers[id]){
      simgrid::s4u::Mailbox *receiver_m = simgrid::s4u::Mailbox::by_name("main"+std::to_string(receiver));
      receiver_m->put(&data,1);
    }
  }


  std::vector<simgrid::s4u::CommPtr> pending_comms;
  while(simgrid::s4u::Engine::get_clock() < 200) {  
    try {
      m->get<int>(10);
      XBT_INFO("%d Received",id);
      if(!forwarded && randlist[run][id]<q){
        forwarded=true;
        XBT_INFO("Forwarding");
        for(auto &receiver: receivers[id]){
          simgrid::s4u::Mailbox *receiver_m = simgrid::s4u::Mailbox::by_name("main"+std::to_string(receiver));
          try{
            simgrid::s4u::CommPtr comm = receiver_m->put_async(&data,1);
            pending_comms.push_back(comm);
          }catch(...){}
        }
      }
    }catch(...){}
  }
  simgrid::s4u::Comm::wait_all(&pending_comms);
}

/**
 * This is the brain behind each node
 */
static void obs_node2(std::vector<std::string> args) {
  const char* c_id=args[0].c_str();
  int id=std::atoi(c_id);
  const char* c_q=args[1].c_str();
  int q=std::atoi(c_q);
  const char* c_initiator=args[2].c_str();
  int initiator=std::atoi(c_initiator);
  simgrid::s4u::Mailbox *m = simgrid::s4u::Mailbox::by_name("main"+args[0]);
  int *data= new int;
  bool forwarded=false;

  simgrid::s4u::this_actor::get_host()->turn_on();
  MODE_IDLE();

  int steps=2000;
  int t=0;
  bool should_forward=false;
  while(simgrid::s4u::Engine::get_clock() <= END_AT){
    bool is_initiator_turn=t/steps<25;
    int current_id=t/steps%25;
    // Compute next step
    t+=steps;
    t/=steps;
    t*=steps;

    if(current_id==id){
      if((!forwarded and should_forward) or (is_initiator_turn and initiator==id)){
        forwarded=true;
        XBT_INFO("Forwarding");

        bool first=true; // Note that in reality, wireless nodes consume for 1 send no matter the number of receivers
        for (int cur_node=0;cur_node<25;cur_node++){
          if(id!=cur_node){
            simgrid::s4u::Mailbox *receiver_m = simgrid::s4u::Mailbox::by_name("main"+std::to_string(cur_node));
            bool *should_receive=new bool;
            *should_receive=(std::find(receivers[id].begin(), receivers[id].end(), cur_node) != receivers[id].end());
            receiver_m->put(should_receive,0);
            if(*should_receive){
              if(first){
                MODE_TX();
                receiver_m->put(&data,DATA_SIZE);
                ntx++;
                MODE_IDLE();
                first=false;
              }
              else{
                receiver_m->put(&data,DATA_SIZE);
              }
            }
          }
        }
      }
    }
    else {
      try{
        bool *should_receive=m->get<bool>(steps);
        if(*should_receive){
          MODE_RX();
          m->get<int>();
          MODE_IDLE()
          XBT_INFO("%d Received",id);
          should_forward=randlist[run][id]<q;
        }
      }catch(...){}
    }

    // Goto next step
    simgrid::s4u::this_actor::sleep_until(t);
  }
}
