#!/usr/bin/env python

# Parameters
bw=50000     # Bandwidth in bps
idle=0.4     # Idle power consumption
tx=0.16      # Tx power consumption

# Clear file if exists
pfile="platform.xml"
f=open(pfile, 'w')
f.close()

# Write to file function
def write(line,tab=True):
    head="\t" if tab else ""
    with open(pfile, 'a') as f:
        f.write(head+line+"\n")

# Header
write('''<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
\t<AS id="AS0" routing="Full">
\t<link id="link" bandwidth="'''+str(bw)+'''bps" latency="0ms" sharing_policy="SHARED"></link>''',tab=False)

# Hosts
for i in range(0,25):
    write('''<host id="n'''+str(i)+'''" speed="100.0f,100.0f,100.0f" pstate="0">''')
    write("\t<prop id=\"wattage_per_state\" value=\"0:0,"+str(idle)+":"+str(idle)+","+str(idle+tx)+":"+str(idle+tx)+"\" />\n\t\t<prop id=\"wattage_off\" value=\"0\" />")
    write('</host>')

# Routes
for i in range(0,25):
    for j in range(0,25):
        write('''<route src="n'''+str(i)+'''" dst="n'''+str(j)+'''" symmetrical="no"><link_ctn id="link"/></route>''')
        
# Footer
write("</AS>\n</platform>")

