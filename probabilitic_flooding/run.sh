#!/usr/bin/env bash

set -e

##### Arguments
nrun=1000

##### Paths
randrun="./randrun.py"
results_sg="results/results_sg.csv"
results_ys="results/results_ys.csv"
results_time="results/runtime.txt"
sim_sg="make -C ./simgrid/ run"
sim_ys="./esds/simulator.py"
rand_sg="random_sg.txt"
rand_ys="random_ys.txt"
seed_ys=15
seed_sg=15

# Generate random streams
$randrun $rand_sg $nrun $seed_sg
$randrun $rand_ys $nrun $seed_ys

# Init results files
echo "q,initiator,seed,run_number,node,energy,received,ntx" > $results_ys
echo "q,initiator,seed,run_number,node,energy,received,ntx" > $results_sg
echo $(date "+%s") > "$results_time"

# Launch simulations
for initiator in $(seq 0 24)
do
    for q in 20 30 40 50 60
    do
        for run in $(seq 0 $(( nrun - 1)))
        do
            # ESDS
            echo "ESDS - Run seed=$seed_ys q=$q run_number=$run initiator=$initiator"
            ys_out=$($sim_ys $run $q $initiator)
            nodes=$(echo "$ys_out" | awk '/Receive.*wlan0/{print $1}' | grep -Po n[0-9]+ | sed "s/^n//g"|sort|uniq|grep -v ^${initiator}$)
            energy=$(echo "$ys_out" | awk '/PowerStatesComms.*consumed/{energy=$5+0;sub("]","",$1);$1=substr($1,match($1,"n[0-9]+"),5);sub("n","",$1);printf($1",%f\n",energy)}')
            ntx=$(echo "$ys_out"|grep -Eo "Number of tx [0-9]+"|awk '{s+=$4} END {print s}')
            for node_id in $(seq 0 24)
            do
                node_energy=$(echo "$energy"|grep "^${node_id},"|cut -d',' -f2)
                received=$(echo "$ys_out"|grep "Receive.*wlan0"| { grep "src=n${node_id}]" || true; })
                [ ! -z "$received" ] && received="true" || received="false"
                [ $initiator == $node_id ] && received="true"
                echo $q,$initiator,$seed_ys,$run,$node_id,$node_energy,$received,$ntx >> $results_ys
            done

            # Simgrid
            sg_out=$($sim_sg run=$run q=$q initiator=$initiator 2>&1)
            echo "Simgrid - Run seed=$seed_sg q=$q run_number=$run initiator=$initiator"
            nodes=$(echo "$sg_out"| awk '/Received/{print $1}' | grep -Po n[0-9]+ | sed "s/^n//g"|sort|uniq|grep -v ^${initiator}$)
            energy=$(echo "$sg_out"| awk '/Energy.* of host/{sub("n","",$7);sub(":","",$7);printf("%d,%f\n",$7,$8+0)}')
            ntx=$(echo "$sg_out"|grep -Eo "Number of tx [0-9]+"|grep -Eo "[0-9]+")
            for node_id in $(seq 0 24)
            do
                node_energy=$(echo "$energy"|grep "^${node_id},"|cut -d',' -f2)
                received=$(echo "$sg_out"|grep "Received"| { grep "\[n${node_id}\:" || true; })
                [ ! -z "$received" ] && received="true" || received="false"
                [ $initiator == $node_id ] && received="true"
                echo $q,$initiator,$seed_ys,$run,$node_id,$node_energy,$received,$ntx >> $results_sg
            done

        done
    done
done


echo $(date "+%s") >> "$results_time"

