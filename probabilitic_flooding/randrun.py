#!/usr/bin/python

import sys, random

filepath=sys.argv[1]
nrun=int(sys.argv[2])
seed=int(sys.argv[3])

random_gen=random.Random(seed)

with open(filepath, "w") as f:
    for i in range(0,nrun):
        for j in range(0,25):
            f.write(str(round(random_gen.uniform(0,1)*100)) + " ")
        f.write('\n')

