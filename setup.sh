#!/usr/bin/bash

# Use SimGrid v3.31
url_simgrid="https://framagit.org/simgrid/simgrid/-/archive/v3.31/simgrid-v3.31.tar.gz"

# Install simgrid
[ ! -e "simgrid.tar.gz" ] && wget "$url_simgrid" -O "simgrid.tar.gz"
tar -xvf "simgrid.tar.gz" && mv "simgrid-v3.31" simgrid && mkdir simgrid/build
cd simgrid/build && cmake ../ && make -j5

# Install ESDS
pip install esds==0.0.3
