#!/usr/bin/env bash

wai=$(dirname $(readlink -f "$0")) # Current script directory
folder="${wai}/../susceptible_infected_recovered/"
maxpar=25 # Choose according to number of cores

ln -sf ../simgrid ./simgrid

for initiator in $(seq 0 24)
do
    cp -r "$folder" ${wai}/${initiator}
    cd ${wai}/${initiator}
    echo $initiator > ./initiator.txt
    ./run_initiator.sh &
    cd -
    [ $(((initiator+1)%maxpar)) -eq 0  ] && wait
done

wait 

echo > runtime.txt
head -n1 0/results/results_ys.csv > results_sg.csv
cp results_sg.csv results_ys.csv
for initiator in $(seq 0 24)
do
    cd ${wai}/${initiator}/
    awk 'NR>1' results/results_sg.csv >> ../results_sg.csv
    awk 'NR>1' results/results_ys.csv >> ../results_ys.csv
    cat results/runtime.txt >> ../runtime.txt
    cd -
    rm -r ${initiator}
done

cat runtime.txt | awk 'BEGIN{min=-1;max=0;} {if($1>max){max=$1+0}if(min<0){min=$1}else{if($1<min){min=$1}}}END{printf("%d\n%d",min,max)}' > runtime2.txt
mv runtime2.txt runtime.txt 

