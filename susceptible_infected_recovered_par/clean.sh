#!/usr/bin/env bash

rm results_*
rm runtime.txt
for i in $(seq 0 24)
do
    rm -rf ./$i
done
